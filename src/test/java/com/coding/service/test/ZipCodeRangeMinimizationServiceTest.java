package com.coding.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

import com.coding.service.impl.ZipCodeRangeMinimization;
import com.coding.service.impl.ZipCodeRangeVO;

@RunWith(JUnit4ClassRunner.class)
public class ZipCodeRangeMinimizationServiceTest {

	private ZipCodeRangeMinimization zipCodeRangeMinimization;

	@Before
	public void setProperties() {
		zipCodeRangeMinimization = new ZipCodeRangeMinimization();

	}
	
	@Test
	public void testMinimizeZipCodeRangeWithEmptyList() {
		
		Assert.assertEquals(new ArrayList<ZipCodeRangeVO>(), zipCodeRangeMinimization
				.minimizeZipCodeRange(new ArrayList<ZipCodeRangeVO>()));
	}
	
	
	@Test
	public void testMinimizeZipCodeRangeWithNonOverlappingRanges() {
		List<ZipCodeRangeVO> finalZipRanges = zipCodeRangeMinimization
				.minimizeZipCodeRange(generateMockDataForNonOverlappingZipRangesTest());
		Assert.assertEquals(generateAssertDataForNonOverlappingZipRangesTest(), finalZipRanges);
	}

	@Test
	public void testMinimizeZipCodeRangeWithOverlappingRanges() {
		List<ZipCodeRangeVO> finalZipRanges = zipCodeRangeMinimization
				.minimizeZipCodeRange(generateMockDataForOverlappingZipRangesTest());
		Assert.assertEquals(generateAssertDataForOverlappingZipRangesTest(), finalZipRanges);
	}

	private List<ZipCodeRangeVO> generateMockDataForNonOverlappingZipRangesTest() {
		List<ZipCodeRangeVO> codeRangeVOs = new ArrayList<>();
		codeRangeVOs.add(new ZipCodeRangeVO(100, 110));
		codeRangeVOs.add(new ZipCodeRangeVO(115, 120));
		codeRangeVOs.add(new ZipCodeRangeVO(90, 98));
		return codeRangeVOs;
	}

	private List<ZipCodeRangeVO> generateAssertDataForNonOverlappingZipRangesTest() {
		List<ZipCodeRangeVO> codeRangeVOs = new ArrayList<>();
		codeRangeVOs.add(new ZipCodeRangeVO(90, 98));
		codeRangeVOs.add(new ZipCodeRangeVO(100, 110));
		codeRangeVOs.add(new ZipCodeRangeVO(115, 120));

		return codeRangeVOs;
	}

	private List<ZipCodeRangeVO> generateMockDataForOverlappingZipRangesTest() {
		List<ZipCodeRangeVO> codeRangeVOs = new ArrayList<>();
		codeRangeVOs.add(new ZipCodeRangeVO(90, 99));
		codeRangeVOs.add(new ZipCodeRangeVO(100, 110));
		codeRangeVOs.add(new ZipCodeRangeVO(115, 120));
		codeRangeVOs.add(new ZipCodeRangeVO(200, 210));
		codeRangeVOs.add(new ZipCodeRangeVO(201, 220));
		return codeRangeVOs;
	}

	private List<ZipCodeRangeVO> generateAssertDataForOverlappingZipRangesTest() {
		List<ZipCodeRangeVO> codeRangeVOs = new ArrayList<>();
		codeRangeVOs.add(new ZipCodeRangeVO(90, 110));
		codeRangeVOs.add(new ZipCodeRangeVO(115, 120));
		codeRangeVOs.add(new ZipCodeRangeVO(200, 220));
		return codeRangeVOs;
	}
}
