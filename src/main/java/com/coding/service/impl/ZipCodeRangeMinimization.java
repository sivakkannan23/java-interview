package com.coding.service.impl;

import java.util.ArrayList;
import java.util.List;

public class ZipCodeRangeMinimization {

	public List<ZipCodeRangeVO> minimizeZipCodeRange(List<ZipCodeRangeVO> zipCodeRangesList)
	{
		List<ZipCodeRangeVO> finalzipCodeRangeVOList = new ArrayList<>();
		
		if(zipCodeRangesList == null || zipCodeRangesList.isEmpty())
		{
			return finalzipCodeRangeVOList;
		}
		
		zipCodeRangesList.sort((ZipCodeRangeVO zipCodeRangeVO1, ZipCodeRangeVO zipCodeRangeVO2) ->  zipCodeRangeVO1.getLowerRange().compareTo(zipCodeRangeVO2.getLowerRange()));
		
		ZipCodeRangeVO tempZipCodeRangeVO = null;
		
		for(ZipCodeRangeVO zipCodeRangeVO: zipCodeRangesList)
		{
			
			if(tempZipCodeRangeVO == null)
			{
				tempZipCodeRangeVO = zipCodeRangeVO;
				finalzipCodeRangeVOList.add(tempZipCodeRangeVO);
				continue;
			}
			
			if(zipCodeRangeVO.getLowerRange() - 1 <= tempZipCodeRangeVO.getUpperRange()
					&& zipCodeRangeVO.getLowerRange() >= tempZipCodeRangeVO.getLowerRange() )
			{
				finalzipCodeRangeVOList.remove(tempZipCodeRangeVO);
				tempZipCodeRangeVO = combineTwoZipRanges(tempZipCodeRangeVO, zipCodeRangeVO);
			} else
			{
				tempZipCodeRangeVO = zipCodeRangeVO;
			}
			
			finalzipCodeRangeVOList.add(tempZipCodeRangeVO);
		}
		
		return finalzipCodeRangeVOList;
		
	}
	
	private ZipCodeRangeVO combineTwoZipRanges(ZipCodeRangeVO zipCodeRangeVO1, ZipCodeRangeVO zipCodeRangeVO2)
	{
		return new ZipCodeRangeVO(zipCodeRangeVO1.getLowerRange(), zipCodeRangeVO2.getUpperRange());
	}
	
	
	
	
	
}
