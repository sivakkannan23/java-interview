package com.coding.service.impl;

import com.google.common.base.Objects;

public class ZipCodeRangeVO {

	private Integer lowerRange;
	private Integer upperRange;

	

	public ZipCodeRangeVO(Integer lowerRange, Integer upperRange) {
		super();
		this.upperRange = upperRange;
		this.lowerRange = lowerRange;
	}

	public Integer getUpperRange() {
		return upperRange;
	}

	public void setUpperRange(Integer upperRange) {
		this.upperRange = upperRange;
	}

	public Integer getLowerRange() {
		return lowerRange;
	}

	public void setLowerRange(Integer lowerRange) {
		this.lowerRange = lowerRange;
	}

	@Override
	public String toString() {
		return "ZipCodeRangeVO [lowerRange=" + lowerRange + ", upperRange=" + upperRange + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.lowerRange) + Objects.hashCode(this.upperRange);
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof ZipCodeRangeVO))
		{
			return false;
		}
		ZipCodeRangeVO codeRangeVO = (ZipCodeRangeVO) obj;
		return Objects.equal(codeRangeVO.getLowerRange(), this.lowerRange) && Objects.equal(codeRangeVO.getUpperRange(), this.upperRange);
	}

	
	
	
	
}
